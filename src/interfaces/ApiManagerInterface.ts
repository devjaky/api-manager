export interface ApiManagerInterface {
  fetchData(): Promise<Response>;
};