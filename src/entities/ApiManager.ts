import { inject, injectable } from "inversify";
import { ApiManagerInterface } from "../interfaces/ApiManagerInterface";
import { TodoClient } from "./TodoClient";
import { TYPES } from "../types/types";

@injectable()
class ApiManager implements ApiManagerInterface {
  private todoClient: TodoClient;

  public constructor(
    @inject(TYPES.TodoClient) todoClient: TodoClient
  ) {
    this.todoClient = todoClient;
  }

  fetchData(): Promise<Response> {
    return this.todoClient.fetchData();
  }
};

export { ApiManager };