import { injectable } from "inversify";
import { TodoInterface } from "../interfaces/TodoInterface";
import fetch from "node-fetch";

@injectable()
class TodoClient implements TodoInterface {
  fetchData(): Promise<Response> {
    return fetch("https://jsonplaceholder.typicode.com/todos").then((result: Response) => result.json());
  }
}

export { TodoClient }