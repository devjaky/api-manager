const TYPES = {
  TodoClient: Symbol.for("TodoClient"),
  ApiManager: Symbol.for("ApiManager")
}

export { TYPES }