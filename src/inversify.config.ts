import { Container } from "inversify";
import "reflect-metadata";

import { TYPES } from "./types/types";
import { TodoClient } from "./entities/TodoClient";
import { ApiManager } from "./entities/ApiManager";

const container = new Container();
container.bind<ApiManager>(TYPES.ApiManager).to(ApiManager);
container.bind<TodoClient>(TYPES.TodoClient).to(TodoClient);

export { container }