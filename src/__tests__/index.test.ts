import { container } from '../inversify.config'
import { TYPES } from '../types/types';
import EXPECTED_DATA from './data.json';
import { ApiManager } from '../entities/ApiManager';
import { TodoClient } from '../entities/TodoClient';

describe("Todo Client", () => {
  let todoClient: TodoClient;
  
  beforeEach(() => {
    todoClient = container.get<TodoClient>(TYPES.TodoClient);
  });

  it("should fetch and return array of users", async () => {
    return todoClient.fetchData().then((result: object) => {
      expect(result).toMatchObject(EXPECTED_DATA);
    });
  });
});

describe("API Manager", () => {
  let apiManager: ApiManager;
  
  beforeEach(() => {
    apiManager = container.get<ApiManager>(TYPES.ApiManager);
  });

  it("should access decorated fetchData from TodoClient", async () => {
    return apiManager.fetchData().then((result: object) => {
      expect(result).toMatchObject(EXPECTED_DATA);
    });
  });
});